/* Copyright (C) 2021 changwei@iscas.ac.cn
* 
* Apache License Version 2.0许可头：
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* 
*    http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
// import vue from 'vue/dist/vue.esm.browser'
// import axios from "axios";
import { checkAuth } from './login/helper';
import Login from './login/Login';
export default ({
  Vue, // VuePress 正在使用的 Vue 构造函数
  options, // 附加到根实例的一些选项
  router, // 当前应用的路由实例
  siteData // 站点元数据
}) => {
  // window.Vue = vue // 使页面中可以使用Vue构造函数 （使页面中的vue demo生效）

  // axios.get('http://localhost:8000/backend/access-stat', '4399message')
  //   .then(response => {
  //     console.log(response.data);
  //   })
  //   .catch(error => {
  //     console.log(error);
  //   });

  Vue.mixin({
    mounted() {
      // console.log(this.$el.baseURI);
      if ("http://localhost:8000/docs/download/".localeCompare(this.$el.baseURI) == 0) {
        // console.log("YESSSS!");
        const doCheck = () => {
          if (!checkAuth()) {
            this.$dlg.modal(Login, {
              width: 300,
              height: 350,
              title: 'Employee login',
              singletonKey: 'employee-login',
              maxButton: false,
              closeButton: false,
              callback: data => {
                if (data === true) {
                  // do some stuff after login
                }
              }
            })
          }
        }
  
       /* if (this.$dlg) {
          doCheck()
        } else {
          import('v-dialogs').then(resp => {
            Vue.use(resp.default)
            this.$nextTick(() => {
              doCheck()
            })
          })
        }*/

      }
    }
  })
}
