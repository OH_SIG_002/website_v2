---
title: 申请SIG
permalink: /SIG/sig-apply
sidebar: false
article: false
comment: false
editLink: false
date: 2021-07-10 12:30:11
---

# 开源共建兴趣组建设规范

## SIG管理章程
SIG（Special Intertest Group）是指特别兴趣小组，SIG在PMC项目管理委员会指导下，负责OpenHarmony社区特定子领域及创新项目的架构设计、开源开发及项目维护等工作。 本目录用于存放OpenHamony社区所有 “特别兴趣小组”（Special Interest Group，以下简称 SIG）的运作信息。

## 申请新建SIG

- 开发者在社区中寻找2-3个有共同兴趣及目标的人，确定SIG Leader。参考<a href="/sig/sig_template_cn/">新建SIG Charter模板</a>，创建SIG Charter提案。
- SIG Leader以[SIG-Charter-Proposal-XXX]为邮件标题，通过向dev@openharmony.io发送邮件，提交新建SIG申请。
- PMC项目管理委员会批准通过后，可创建新的SIG。

## 加入已有SIG
开发者可通过SIG列表查看感兴趣的SIG，通过订阅邮件列表、参与SIG会议等形式，参与对应SIG项目的技术讨论、社区维护及开源开发。

## 运营维护SIG

- SIG Leader Fork OpenHamony/community分支，在SIG文件夹下，以新SIG名称新建文件夹，并参考<a href="/sig/sig_template_cn/" >SIG模板</a>，创建对应的SIG配置文件，并提交PR合入申请。
- SIG孵化子项目，统一存放在<a href="https://gitee.com/openharmony-sig" target="_blank">OpenHarmony SIG组织</a>，待孵化成熟后，可合入OpenHarmony组织代码主库。
- SIG Leader及Committer负责对应SIG的运营及维护。
- SIG Leader定期在PMC项目管理委员会汇报SIG孵化项目及SIG运营进展，PMC基于SIG运作情况给出指导建议。

## SIG孵化项目毕业
- SIG孵化项目成熟并满足项目毕业要求后，可申请合入OpenHarmony组织代码主库。
- SIG Leader通过向dev@openharmony.io发送邮件，提交孵化项目毕业申请。
- PMC项目管理委员会通过项目毕业申请后，社区接纳孵化项目合入OpenHarmony主干。
更多SIG资料，请访问https://gitee.com/openharmony/community/tree/master/sig。
