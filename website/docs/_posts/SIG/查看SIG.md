---
title: 查看SIG
permalink: /SIG/sigs
sidebar: false
article: false
comment: false
editLink: false
date: 2021-07-10 12:30:11
---
## SIG 组介绍
1. SIG（Special Interest Group）是指特别兴趣小组，SIG在PMC项目管理委员会指导下，负责OpenHarmony社区特定子领域及创新项目的架构设计、开源开发及项目维护等工作。
2. 为了便于OpenHarmony开源社区工作开展和交流，默认将其划分为23个[初始的SIG小组](/SIG/sig_subsystem_list/)。
3. 本目录用于存放OpenHamony社区所有 “特别兴趣小组”（Special Interest Group，以下简称 SIG）的运作信息。

## SIG职责&运作方式
1. 领域的技术演进方向由SIG组承担，负责领域技术竞争力分析和关键技术识别及决策。
2. 负责领域内功能分解分配，模块间接口定义与维护管理。
3. 社区的工作实体是SIG组，从基础设施到OS部件，从测试系统到版本发布都是由不同SIG的来承担。
4. 一个良好的社区组织形式是持续运作的关键，仪式感很重要，SIG组需要通过周期例会来保持其有效的运作，并定期向PMC委员会汇报进展。

## SIG List

|  SIG 名称   | Gitee主页  |  邮件列表   | 状态 |
|  ----  | ----  |  ----  | ----  |
| <a href="/sigs/riscv/">RISC-V</a>  | <a href="https://gitee.com/openharmony-sig/riscv">Gitee</a> |  <a href="mailto:sig_risc_v@openharmony.io">sig_risc_v@openharmony.io</a> | 已成立 |
| <a href="/sigs/devboard/">DevBoard</a>  | <a href="https://gitee.com/openharmony/community/tree/master/sig/sig-devboard">Gitee</a> |  <a href="mailto:sig_devboard@openharmony.io">sig_devboard@openharmony.io</a> | 已成立 | 
| <a href="/sigs/openblock/">OpenBlock</a>  | <a href="https://gitee.com/openharmony-sig/openblock">Gitee</a> |  <a href="mailto:sig_dllite_micro@openharmony.io">sig_dllite_micro@openharmony.io</a> | 已成立 | 
| <a href="/sigs/sysapp/">System Applications</a>  | <a href="https://gitee.com/openharmony-sig/system_applications">Gitee</a> |  <a href="mailto:sig_systemapplication@openharmony.io">sig_systemapplication@openharmony.io</a> | 已成立 |
| <a href="/sigs/dlmicro/">DL-Lite-Micro</a>  | <a href="https://gitee.com/openharmony-sig/dllite_micro">Gitee</a> |  <a href="mailto:">sig_risc_v@openharmony.io</a> | 已成立 | 
| <a href="/sigs/python/">Python</a>  | <a href="https://gitee.com/openharmony-sig/python">Gitee</a> |  <a href="mailto:sig_python@openharmony.io">sig_python@openharmony.io</a> | 已成立 |
| <a href="/sigs/linkboy/">Linkboy</a>  | <a href="https://gitee.com/openharmony-sig/linkboy">Gitee</a> |  <a href="mailto:sig_linkboy@openharmony.io">sig_linkboy@openharmony.io</a> | 已成立 | 

## 申请 SIG
目前SIG组的成立我们是辅导制, 由用户社区的一名导师给予开发者初期一定的指导并辅助开发者完成SIG的设立工作。如果希望成立SIG 或希望了解相关事项, 请去[社区主页](/community/#社区交流)与我们的社区导师取得联系.
* [minglong](mailto:minglong@iscas.ac.cn)
* [likai](mailto:likai20@iscas.ac.cn)
* [longjun](mailto:longjun@iscas.ac.cn)