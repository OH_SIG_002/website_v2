---
title: UnionPi
permalink: /supported_devices/UnionPi
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-11 17:56:59
---
# UnionPi 

* 底板正面
<img src="/devices/UnionPi/figures/底板_核心板.png">

* 底板背面
<img src="/devices/UnionPi/figures/扩展板背面.png">

* 核心板正面
<img src="/devices/UnionPi/figures/核心板.png">

* 核心板背面
<img src="/devices/UnionPi/figures/核心板背面.png">

# 芯片参数

* Soc   
  
    Amlogic A311D

* CPU
  
    4x Cortex-A73 up to 2.2 GHz

    2x Cortex-A53 up to 1.8 GHz

* GPU

    ARM Mali-G52 4核GPU

    支持OpenGL ES 3.2，OpenCL 2.0

* NPU
  
    支持 8bit/16bit 运算，运算性能高达 5.0TOPS
    
    支持Tengine AI处理框架

* VPU

    支持4K VP9，H265，H264视频解码，高达60fps

    支持多路1080P 多格式解码（H265，H264，MPEG-1/2/4，VP9，WMV） 

    一路1080P@60fps H264,H265编码 
    
    一路JPEG编码

* RAM

    LPDDR4x 4GB

* ROM

    高速 eMMC 5.1 32GB

# 外设

* Ethernet Port 
  
    1000 Mbps（RJ45）

* Wireless  

    可接入4G模块

* Display                           

    1x HDMI 2.0

    1x MIPI-DSI

* Audio
  
    1x HDMI 2.0
    
    1x Speaker 双声道（4Ω/5W）
    
    1x 四段式CTIA耳麦，支持麦克风音频输入

* USB

    3x USB2.0 Hub

    1x USB3.0

    1x TYPE-C(OTG)

* Camera

    1x 2-lane MIPI-CSI 摄像头接口


* RS485 X1
  
* CAN Bus X1

* ADC X1

* PWM X1
  
* TTL X1
  
* GPIO X18

* Power Source DC 12V/3A
  
# 其他

* OpenHarmony 兼容性认证（进行中）
* [Software](https://gitee.com/openharmony-sig/device_unionpi.git)                             
* [Hardware](https://www.aliyundrive.com/s/p6USpMydhWJ)
* [Support](https://gitee.com/openharmony-sig/device_unionpi/issues)
