---
title: 成员单位
date: 2021-10-23 02:40:56
sidebar: false
article: false
comment: false
editLink: false
permalink: /members
---
## OpenHarmony 开源项目
OpenHarmony是由开放原子开源基金会（OpenAtom Foundation）孵化及运营的开源项目，目标是面向全场景、全连接、全智能时代，基于开源的方式，搭建一个智能终端设备操作系统的框架和平台，促进万物互联产业的繁荣发展。

愿景：打造开放的、全球化的、创新且领先的面向多智能终端、全场景的分布式操作系统，构筑可持续发展的开源生态系统。

使命：OpenHarmony项目群托管了操作系统技术和架构的核心代码及组件。以开放治理的方式聚合芯片开发者、方案开发者、产品开发者、应用开发者及各种使能者。持续发展代码使用者和共建者。

## 捐赠人
<Members></Members>
