---
title: subsys-data
permalink: /pages/extra/c92e1f/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 数据管理

-   **[关系型数据库](/pages/extra/4b14bb/)**  
-   **[轻量级数据存储](/pages/extra/829325/)**  


