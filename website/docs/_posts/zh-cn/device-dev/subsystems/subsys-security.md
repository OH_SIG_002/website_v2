---
title: subsys-security
permalink: /pages/extra/2dd473/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 安全<a name="ZH-CN_TOPIC_0000001157319395"></a>

-   **[概述](/pages/01050d01)**  

-   **[应用验签开发指导](/pages/01050d02)**  

-   **[应用权限管理开发指导](/pages/01050d03)**  

-   **[IPC通信鉴权开发指导](/pages/01050d04)**  


