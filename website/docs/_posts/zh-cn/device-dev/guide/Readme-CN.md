---
title: Readme-CN
permalink: /pages/extra/b89d59/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 设备开发指南

- [轻量和小型系统设备](/pages/extra/4cd9dc/)
  - [WLAN连接类产品](/pages/extra/a9f9b7/)
    - [LED外设控制](/pages/0107010101)
    - [集成三方SDK](/pages/0107010102)
  - [无屏摄像头类产品](/pages/extra/3b686a/)
    - [摄像头控制](/pages/extra/b2b869/)
      - [概述](/pages/010701020101)
      - [示例开发](/pages/extra/d23bd1/)
        - [拍照开发指导](/pages/01070102010201)
        - [录像开发指导](/pages/01070102010202)
      - [应用实例](/pages/010701020103)
  - [带屏摄像头类产品](/pages/extra/51c9a4/)
    - [屏幕和摄像头控制](/pages/extra/ac3f29/)
      - [概述](/pages/010701030101)
      - [示例开发](/pages/extra/781d25/)
        - [拍照开发指导](/pages/01070103010201)
        - [录像开发指导](/pages/01070103010202)
        - [预览开发指导](/pages/01070103010203)
      - [应用实例](/pages/010701030103)
    - [视觉应用开发](/pages/extra/d06f24/)
      - [概述](/pages/010701030201)
      - [开发准备](/pages/010701030202)
      - [添加页面](/pages/010701030203)
      - [开发首页](/pages/010701030204)
      - [开发详情页](/pages/010701030205)
      - [调试打包](/pages/010701030206)
      - [真机运行](/pages/010701030207)
      - [常见问题](/pages/010701030208)
- [标准系统设备](/pages/extra/c37a91/)
  - [时钟应用开发指导](/pages/01070201)
  - [平台驱动开发示例](/pages/01070202)
  - [外设驱动开发示例](/pages/01070203)