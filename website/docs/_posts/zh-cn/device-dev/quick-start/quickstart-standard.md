---
title: quickstart-standard
permalink: /pages/extra/6dca5d/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 标准系统入门<a name="ZH-CN_TOPIC_0000001111221726"></a>

-   **[标准系统入门简介](/pages/01020201)**  

-   **[标准系统开发环境准备（仅Hi3516需要）](/pages/01020202)**  

-   **[获取源码](/pages/01020203)**  

-   **[运行“Hello World”](/pages/extra/743c79/)**  

-   **[常见问题](/pages/01020205)**  

-   **[附录](/pages/extra/f03f91/)**  


