---
title: quickstart-lite-env-setup
permalink: /pages/extra/770961/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 搭建轻量与小型系统环境<a name="ZH-CN_TOPIC_0000001216535391"></a>

-   **[搭建系统环境概述](/pages/0102010201)**  

-   **[开发环境准备](/pages/0102010202)**  

-   **[获取源码](/pages/0102010203)**  

-   **[使用安装包方式搭建编译环境](/pages/0102010204)**  

-   **[使用Docker方式搭建编译环境](/pages/0102010205)**  

-   **[常见问题](/pages/0102010206)**  


