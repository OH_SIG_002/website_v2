---
title: quickstart-lite-steps-hi3861
permalink: /pages/extra/d8f8db/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# Hi3861开发板<a name="ZH-CN_TOPIC_0000001171774078"></a>

-   **[安装开发板环境](/pages/010201030101)**  

-   **[新建应用程序](/pages/010201030102)**  

-   **[编译](/pages/010201030103)**  

-   **[烧录](/pages/010201030104)**  

-   **[调试验证](/pages/010201030105)**  

-   **[运行](/pages/010201030106)**  

-   **[常见问题](/pages/010201030107)**  


