---
title: kernel-small-bundles-fs-support
permalink: /pages/extra/436bd9/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 支持的文件系统<a name="ZH-CN_TOPIC_0000001124888127"></a>

-   **[FAT](/pages/0105010204050201)**  

-   **[JFFS2](/pages/0105010204050202)**  

-   **[NFS](/pages/0105010204050203)**  

-   **[Ramfs](/pages/0105010204050204)**  

-   **[Procfs](/pages/0105010204050205)**  


