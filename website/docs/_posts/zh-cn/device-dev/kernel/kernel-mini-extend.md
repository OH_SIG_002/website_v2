---
title: kernel-mini-extend
permalink: /pages/extra/c07c37/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 扩展组件<a name="ZH-CN_TOPIC_0000001123863139"></a>

-   **[C++支持](/pages/010501010301)**  

-   **[CPU占用率](/pages/extra/3ca78d/)**  

-   **[动态加载](/pages/extra/a97ca7/)**  

-   **[文件系统](/pages/extra/2bbbd9/)**  


