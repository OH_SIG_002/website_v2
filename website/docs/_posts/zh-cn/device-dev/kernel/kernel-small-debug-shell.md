---
title: kernel-small-debug-shell
permalink: /pages/extra/36f10b/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# Shell<a name="ZH-CN_TOPIC_0000001179845911"></a>

-   **[Shell介绍](/pages/01050102050101)**  

-   **[Shell命令开发指导](/pages/01050102050102)**  

-   **[Shell命令编程实例](/pages/01050102050103)**  

-   **[Shell命令使用详解](/pages/extra/9729b4/)**  

-   **[魔法键使用方法](/pages/01050102050105)**  

-   **[用户态异常信息说明](/pages/01050102050106)**  


