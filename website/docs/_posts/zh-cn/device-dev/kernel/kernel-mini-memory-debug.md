---
title: kernel-mini-memory-debug
permalink: /pages/extra/034b9c/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 内存调测<a name="ZH-CN_TOPIC_0000001079036426"></a>

内存调测方法旨在辅助定位动态内存相关问题，提供了基础的动态内存池信息统计手段，向用户呈现内存池水线、碎片率等信息；提供了内存泄漏检测手段，方便用户准确定位存在内存泄漏的代码行，也可以辅助分析系统各个模块内存的使用情况；提供了踩内存检测手段，可以辅助定位越界踩内存的场景。

-   **[内存信息统计](/pages/01050101040101)**  

-   **[内存泄漏检测](/pages/01050101040102)**  

-   **[踩内存检测](/pages/01050101040103)**  


