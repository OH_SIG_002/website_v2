---
title: kernel-small-basics
permalink: /pages/extra/365208/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 基础内核<a name="ZH-CN_TOPIC_0000001123091601"></a>

-   **[中断及异常处理](/pages/010501020301)**  

-   **[进程管理](/pages/extra/97e28a/)**  

-   **[内存管理](/pages/extra/e65a3e/)**  

-   **[内核通信机制](/pages/extra/1d11d4/)**  

-   **[时间管理](/pages/010501020305)**  

-   **[软件定时器](/pages/010501020306)**  

-   **[原子操作](/pages/010501020307)**  


