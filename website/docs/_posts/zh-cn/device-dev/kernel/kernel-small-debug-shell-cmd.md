---
title: kernel-small-debug-shell-cmd
permalink: /pages/extra/810976/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 系统命令<a name="ZH-CN_TOPIC_0000001179965831"></a>

-   **[cpup](/pages/010501020501040101)**  

-   **[date](/pages/010501020501040102)**  

-   **[dmesg](/pages/010501020501040103)**  

-   **[du](/pages/010501020501040216)**  

-   **[exec](/pages/010501020501040104)**  

-   **[free](/pages/010501020501040105)**  

-   **[help](/pages/010501020501040106)**  

-   **[hwi](/pages/010501020501040107)**  

-   **[kill](/pages/010501020501040108)**  

-   **[log](/pages/010501020501040109)**  

-   **[memcheck](/pages/01050102050104010a)**  

-   **[oom](/pages/01050102050104010b)**  

-   **[pmm](/pages/01050102050104010c)**  

-   **[reboot](/pages/010501020501040117)**  

-   **[reset](/pages/01050102050104010d)**  

-   **[sem](/pages/01050102050104010e)**  

-   **[stack](/pages/01050102050104010f)**  

-   **[su](/pages/010501020501040110)**  

-   **[swtmr](/pages/010501020501040111)**  

-   **[systeminfo](/pages/010501020501040112)**  

-   **[task](/pages/010501020501040113)**  

-   **[top](/pages/010501020501040118)**  

-   **[uname](/pages/010501020501040114)**  

-   **[vmm](/pages/010501020501040115)**  

-   **[watch](/pages/010501020501040116)**  
