---
title: kernel-small-apx-structure
permalink: /pages/extra/1f9e6d/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 基本数据结构<a name="ZH-CN_TOPIC_0000001078876282"></a>

-   **[双向链表](/pages/01050102060101)**  

-   **[位操作](/pages/01050102060102)**  


