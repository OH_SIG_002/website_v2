---
title: porting
permalink: /pages/extra/a8791f/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 移植<a name="ZH-CN_TOPIC_0000001157479393"></a>

-   **[三方库移植指导](/pages/extra/e80fe1/)**  

-   **[轻量系统芯片移植指导](/pages/extra/bc2a1e/)**  

-   **[小型系统芯片移植指导](/pages/extra/362558/)**  

-   **[标准系统移植指导](/pages/01040301)**  


