---
title: porting-thirdparty
permalink: /pages/extra/e80fe1/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 轻量和小型系统三方库移植指导<a name="ZH-CN_TOPIC_0000001157479373"></a>

-   **[概述](/pages/01040401)**  

-   **[CMake方式组织编译的库移植](/pages/01040402)**  

-   **[Makefile方式组织编译的库移植](/pages/01040403)**  


