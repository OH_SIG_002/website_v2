---
title: porting-minichip
permalink: /pages/extra/bc2a1e/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 轻量系统芯片移植指导<a name="ZH-CN_TOPIC_0000001157479383"></a>

-   **[移植准备](/pages/extra/2bf51a/)**  

-   **[内核移植](/pages/extra/7a2f63/)**  

-   **[板级系统移植](/pages/extra/9257d8/)**  

-   **[常见问题](/pages/01040104)**  


