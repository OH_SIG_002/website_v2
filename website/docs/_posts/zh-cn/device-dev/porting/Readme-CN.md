---
title: Readme-CN
permalink: /pages/extra/6a00bc/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
## 概述
目前OpenHarmony已经成立了SIG组[sig-devboard](https://gitee.com/openharmony/community/blob/master/sig/sig-devboard/sig_devboard_cn.md)。该SIG组以支持更多第三方开发板为目标，提供开发板移植的支撑。

在了解开发板移植前，需要先了解一下OpenHarmony对设备的分类。不同设备类型的移植方法会有较大差异。

| 设备类型    | 硬件要求        | 支持的内核          |
|---------|-------------|----------------|
| 轻量系统类设备 | 内存>128KB    | LiteOS-M       |
| 小型系统类设备 | 内存>1MB、有MMU | LiteOS-A、Linux |
| 标准系统类设备 | 内存>128MB    |  Linux       |

## 代码准备

目前OpenHarmony已经为各厂家创建了仓库并在openharmony-sig中进行孵化。参与孵化仓开发，需要使用如下方法初始化和下载代码。

```shell
repo init -u https://gitee.com/openharmony-sig/manifest.git -b master -m devboard.xml --no-repo-verify
```

其他下载步骤与主线相同。

## 芯片移植指导

- [轻量系统芯片移植指导](/pages/extra/bc2a1e/)
  - [移植准备](/pages/extra/2bf51a/)
    - [移植须知](/pages/0104010101)
    - [编译构建适配流程](/pages/0104010102)
  - [内核移植](/pages/extra/7a2f63/)
    - [移植概述](/pages/0104010201)
    - [内核基础适配](/pages/0104010202)
    - [内核移植验证](/pages/0104010203)
  - [板级系统移植](/pages/extra/9257d8/)
    - [移植概述](/pages/0104010301)
    - [板级驱动适配](/pages/0104010302)
    - [HAL层实现](/pages/0104010303)
    - [系统组件调用](/pages/0104010304)
    - [lwIP组件适配](/pages/0104010305)
    - [三方组件适配](/pages/0104010306)
    - [XTS认证](/pages/0104010307)
  - [常见问题](/pages/01040104)
- [小型系统芯片移植指导](/pages/extra/362558/)
  - [移植准备](/pages/extra/f4e07b/)
    - [移植须知](/pages/0104020101)
    - [编译构建](/pages/0104020102)
  - [移植内核](/pages/extra/2858ab/)
    - [LiteOS-A内核](/pages/0104020201)
    - [Linux内核](/pages/0104020202)
  - [驱动移植](/pages/extra/f63f6f/)
    - [移植概述](/pages/0104020301)
    - [平台驱动移植](/pages/0104020302)
    - [器件驱动移植](/pages/0104020303)
- [标准系统芯片移植指导](/pages/01040301)
    - [标准系统移植指南](/pages/01040301)
    - [一种快速移植OpenHarmony Linux内核的方法](/pages/01040302)

    - [概述](/pages/01040401)
    - [CMake方式组织编译的库移植](/pages/01040402)
    - [Makefile方式组织编译的库移植](/pages/01040403)

## 芯片移植案例

- [轻量系统芯片移植案例](/pages/extra/890e18/)
    - [带屏解决方案之恒玄芯片移植案例](/pages/01040501)

