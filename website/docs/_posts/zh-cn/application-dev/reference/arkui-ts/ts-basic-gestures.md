---
title: ts-basic-gestures
permalink: /pages/extra/51a545/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 基础手势<a name="ZH-CN_TOPIC_0000001192915098"></a>

-   **[TapGesture](/pages/010c02020101030201)**  

-   **[LongPressGesture](/pages/010c02020101030202)**  

-   **[PanGesture](/pages/010c02020101030203)**  

-   **[PinchGesture](/pages/010c02020101030204)**  

-   **[RotationGesture](/pages/010c02020101030205)**  

-   **[SwipeGesture](/pages/010c02020101030206)**  


