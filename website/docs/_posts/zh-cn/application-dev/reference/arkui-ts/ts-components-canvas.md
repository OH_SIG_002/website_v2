---
title: ts-components-canvas
permalink: /pages/extra/f2014a/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 画布组件<a name="ZH-CN_TOPIC_0000001192595180"></a>

-   **[Canvas](/pages/010c0202010501)**  

-   **[CanvasRenderingContext2D对象](/pages/010c0202010502)**  

-   **[OffscreenCanvasRenderingConxt2D对象](/pages/010c0202010503)**  

-   **[Lottie](/pages/010c0202010504)**  

-   **[Path2D对象](/pages/010c0202010505)**  

-   **[CanvasGradient对象](/pages/010c0202010506)**  

-   **[ImageBitmap对象](/pages/010c0202010507)**  

-   **[ImageData对象](/pages/010c0202010508)**  


