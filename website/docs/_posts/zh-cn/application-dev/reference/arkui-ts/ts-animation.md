---
title: ts-animation
permalink: /pages/extra/e7546b/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 动画<a name="ZH-CN_TOPIC_0000001237355053"></a>

-   **[属性动画](/pages/010c02020201)**  

-   **[显式动画](/pages/010c02020202)**  

-   **[转场动画](/pages/extra/242c24/)**  

-   **[路径动画](/pages/010c02020204)**  

-   **[矩阵变换](/pages/010c02020205)**  

-   **[插值计算](/pages/010c02020206)**  


