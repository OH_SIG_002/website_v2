---
title: js-components-basic
permalink: /pages/extra/d6389e/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 基础组件<a name="ZH-CN_TOPIC_0000001127284922"></a>

-   **[button](/pages/010c0201010301)**  

-   **[chart](/pages/010c0201010302)**  

-   **[divider](/pages/010c0201010303)**  

-   **[image](/pages/010c0201010304)**  

-   **[image-animator](/pages/010c0201010305)**  

-   **[input](/pages/010c0201010306)**  

-   **[label](/pages/010c0201010307)**  

-   **[marquee](/pages/010c0201010308)**  

-   **[menu](/pages/010c0201010309)**  

-   **[option](/pages/010c020101030a)**  

-   **[picker](/pages/010c020101030b)**  

-   **[picker-view](/pages/010c020101030c)**  

-   **[piece](/pages/010c020101030d)**  

-   **[progress](/pages/010c020101030e)**  

-   **[qrcode](/pages/010c020101030f)**  

-   **[rating](/pages/010c0201010310)**  

-   **[richtext](/pages/010c0201010311)**  

-   **[search](/pages/010c0201010312)**  

-   **[select](/pages/010c0201010313)**  

-   **[slider](/pages/010c0201010314)**  

-   **[span](/pages/010c0201010315)**  

-   **[switch](/pages/010c0201010316)**  

-   **[text](/pages/010c0201010317)**  

-   **[textarea](/pages/010c0201010318)**  

-   **[toolbar](/pages/010c0201010319)**  

-   **[toolbar-item](/pages/010c020101031a)**  

-   **[toggle](/pages/010c020101031b)**  


