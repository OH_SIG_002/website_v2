---
title: js-components-common
permalink: /pages/extra/1ce3c8/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 通用<a name="ZH-CN_TOPIC_0000001127284874"></a>

-   **[通用属性](/pages/010c0201010101)**  

-   **[通用样式](/pages/010c0201010102)**  

-   **[通用事件](/pages/010c0201010103)**  

-   **[通用方法](/pages/010c0201010104)**  

-   **[动画样式](/pages/010c0201010105)**  

-   **[渐变样式](/pages/010c0201010106)**  

-   **[转场样式](/pages/010c0201010107)**  

-   **[媒体查询](/pages/010c0201010108)**  

-   **[自定义字体样式](/pages/010c0201010109)**  

-   **[原子布局](/pages/010c020101010a)**  


