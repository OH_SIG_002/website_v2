---
title: js-components-canvas
permalink: /pages/extra/c2a5d2/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 画布组件<a name="ZH-CN_TOPIC_0000001173164681"></a>

-   **[canvas组件](/pages/010c0201010501)**  

-   **[CanvasRenderingContext2D对象](/pages/010c0201010502)**  

-   **[Image对象](/pages/010c0201010503)**  

-   **[CanvasGradient对象](/pages/010c0201010504)**  

-   **[ImageData对象](/pages/010c0201010505)**  

-   **[Path2D对象](/pages/010c0201010506)**  

-   **[ImageBitmap对象](/pages/010c0201010507)**  

-   **[OffscreenCanvas对象](/pages/010c0201010508)**  

-   **[OffscreenCanvasRenderingContext2D对象](/pages/010c0201010509)**  


