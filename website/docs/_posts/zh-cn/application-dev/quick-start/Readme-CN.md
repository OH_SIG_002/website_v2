---
title: Readme-CN
permalink: /pages/extra/82bbc8/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 入门

- DevEco Studio（OpenHarmony）使用指南
    - [概述](/pages/01080901)
    - [版本变更说明](/pages/01080902)
    - [配置OpenHarmony SDK](/pages/01080903)
    - 创建OpenHarmony工程
        - [使用工程向导创建新工程](/pages/0108090401)
        - [通过导入Sample方式创建新工程](/pages/0108090402)
    - [配置OpenHarmony应用签名信息](/pages/01080905)
    - [安装运行OpenHarmony应用](/pages/01080906)
- [包结构说明](/pages/010c03)
- 快速入门
    - [开发准备](/pages/01080101)
    - [使用JS语言开发](/pages/01080102)
    - [使用eTS语言开发](/pages/01080103)
