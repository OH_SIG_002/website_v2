---
title: Readme-CN
permalink: /pages/extra/31858c/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 应用开发

- [应用开发导读](/pages/extra/e59705/)
- [DevEco Studio（OpenHarmony）使用指南](/pages/extra/d114c8/)
- [包结构说明](/pages/010c03)
- [快速入门](/pages/extra/330eee/)
- [Ability框架](/pages/extra/f24f79/)
- 方舟开发框架（ArkUI）
    -  [基于JS扩展的类Web开发范式](/pages/extra/cdf053/)
    -  [基于TS扩展的声明式开发范式](/pages/extra/da8c59/)
- [后台代理提醒](/pages/extra/dff9d1/)
- [后台任务管理](/pages/extra/cc57fa/)
- [媒体](/pages/extra/504ad4/)
- [安全](/pages/extra/85ba77/)
- [IPC与RPC通信](/pages/extra/c25112/)
- [数据管理](/pages/extra/ebcb6a/)
- [USB服务](/pages/extra/6eccf9/)
- [DFX](/pages/extra/0b29a3/)
- [开发参考](/pages/extra/23166e/)

