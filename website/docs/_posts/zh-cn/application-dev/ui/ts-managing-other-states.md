---
title: ts-managing-other-states
permalink: /pages/extra/9470b7/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 其他类目的状态管理<a name="ZH-CN_TOPIC_0000001131511284"></a>

-   **[Observed和ObjectLink数据管理](/pages/0108020303030401)**  

-   **[@Consume和@Provide数据管理](/pages/0108020303030402)**  

-   **[@Watch](/pages/0108020303030403)**  


