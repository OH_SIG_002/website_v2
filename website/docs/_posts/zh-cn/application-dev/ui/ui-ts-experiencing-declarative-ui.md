---
title: ui-ts-experiencing-declarative-ui
permalink: /pages/extra/110365/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 体验声明式UI



- **[创建声明式UI工程](/pages/010802030401)**

- **[初识Component](/pages/010802030402)**

- **[创建简单视图](/pages/010802030403)**