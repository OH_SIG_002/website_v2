---
title: ts-managing-component-states
permalink: /pages/extra/b7881d/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 管理组件拥有的状态<a name="ZH-CN_TOPIC_0000001157388859"></a>

-   **[@State](/pages/0108020303030201)**  

-   **[@Prop](/pages/0108020303030202)**  

-   **[@Link](/pages/0108020303030203)**  


