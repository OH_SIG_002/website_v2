---
title: ui-arkui-ts
permalink: /pages/extra/da8c59/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 基于TS扩展的声明式开发范式



- **[概述](/pages/0108020301)**

- **[框架说明](/pages/extra/638001/)**

- **[声明式语法](/pages/extra/19a335/)**

- **[体验声明式UI](/pages/extra/110365/)**

- **[页面布局与连接](/pages/extra/f9c0e8/)**