---
title: OpenHarmony社区 Committers 列表
sidebar: true
article: true
comment: false
editLink: false
permalink: /community/committers
date: 2021-08-06 02:05:46
---
|编号|gitee账号 |gitee关联邮箱|
|:----: |:----: |:----: |
|1|[https://gitee.com/ailorna](https://gitee.com/ailorna)|[hehuan1@huawei.com](mailto:hehuan1@huawei.com)|
|2|[https://gitee.com/aqxyjay](https://gitee.com/aqxyjay)|[zhangchunxin@huawei.com](mailto:zhangchunxin@huawei.com)|
|3|[https://gitee.com/autumn330](https://gitee.com/autumn330)|[hw.liuwei@huawei.com](mailto:hw.liuwei@huawei.com)|
|4|[https://gitee.com/bigpumpkin](https://gitee.com/bigpumpkin)|[chenzhennan@huawei.com](mailto:chenzhennan@huawei.com)|
|5|[https://gitee.com/bj1010](https://gitee.com/bj1010)|[dubingjian@huawei.com](mailto:dubingjian@huawei.com)|
|6|[https://gitee.com/blancwu](https://gitee.com/blancwu)|[wuyawei1@huawei.com](mailto:wuyawei1@huawei.com)|
|7|[https://gitee.com/borne](https://gitee.com/borne)|[bo.lu@huawei.com](mailto:bo.lu@huawei.com)|
|8|[https://gitee.com/brickhz](https://gitee.com/brickhz)|[zhujianjian1@huawei.com](mailto:zhujianjian1@huawei.com)|
|9|[https://gitee.com/buranfanchen](https://gitee.com/buranfanchen)|[wangjuntao.wang@huawei.com](mailto:wangjuntao.wang@huawei.com)|
|10|[https://gitee.com/Chaos-Liang](https://gitee.com/Chaos-Liang)|[liangguanchao1@huawei.com](mailto:liangguanchao1@huawei.com)|
|11|[https://gitee.com/cheng_guohong](https://gitee.com/cheng_guohong)|[guohong.cheng@huawei.com](mailto:guohong.cheng@huawei.com)|
|12|[https://gitee.com/clevercong](https://gitee.com/clevercong)|[lichunlin2@huawei.com](mailto:lichunlin2@huawei.com)|
|13|[https://gitee.com/davidwulanxi](https://gitee.com/davidwulanxi)|[wuyonghui9@huawei.com](mailto:wuyonghui9@huawei.com)|
|14|[https://gitee.com/defeng2020](https://gitee.com/defeng2020)|[wudefeng@huawei.com](mailto:wudefeng@huawei.com)|
|15|[https://gitee.com/dongjinguang](https://gitee.com/dongjinguang)|[dongjinguang@huawei.com](mailto:dongjinguang@huawei.com)|
|16|[https://gitee.com/fma66169](https://gitee.com/fma66169)|[mazhanfu@huawei.com](mailto:mazhanfu@huawei.com)|
|17|[https://gitee.com/gaohanyi1982](https://gitee.com/gaohanyi1982)|[gaohanyi@huawei.com](mailto:gaohanyi@huawei.com)|
|18|[https://gitee.com/godmiaozi](https://gitee.com/godmiaozi)|[pengbiao1@huawei.com](mailto:pengbiao1@huawei.com)|
|19|[https://gitee.com/hada__lee](https://gitee.com/hada__lee)|[lifu2@huawei.com](mailto:lifu2@huawei.com)|
|20|[https://gitee.com/handyohos](https://gitee.com/handyohos)|[zhangxiaotian@huawei.com](mailto:zhangxiaotian@huawei.com)|
|21|[https://gitee.com/hhh2](https://gitee.com/hhh2)|[pengfei.hou@huawei.com](mailto:pengfei.hou@huawei.com)|
|22|[https://gitee.com/huanghuijin](https://gitee.com/huanghuijin)|[huanghuijin@huawei.com](mailto:huanghuijin@huawei.com)|
|23|[https://gitee.com/hujun211](https://gitee.com/hujun211)|[hujun211@huawei.com](mailto:hujun211@huawei.com)|
|24|[https://gitee.com/hwlitao](https://gitee.com/hwlitao)|[litao25@huawei.com](mailto:litao25@huawei.com)|
|25|[https://gitee.com/illybyy](https://gitee.com/illybyy)|[baoyayong@huawei.com](mailto:baoyayong@huawei.com)|
|26|[https://gitee.com/jalenchen](https://gitee.com/jalenchen)|[chenyaxun@huawei.com](mailto:chenyaxun@huawei.com)|
|27|[https://gitee.com/jameshw](https://gitee.com/jameshw)|[jameslee@huawei.com](mailto:jameslee@huawei.com)|
|28|[https://gitee.com/jiang-xiaofeng](https://gitee.com/jiang-xiaofeng)|[jiangxiaofeng8@huawei.com](mailto:jiangxiaofeng8@huawei.com)|
|29|[https://gitee.com/jyh926](https://gitee.com/jyh926)|[jiayanhong@huawei.com](mailto:jiayanhong@huawei.com)|
|30|[https://gitee.com/karl-z](https://gitee.com/karl-z)|[zhangyongzhi@huawei.com](mailto:zhangyongzhi@huawei.com)|
|31|[https://gitee.com/kkup180](https://gitee.com/kkup180)|[likailong@huawei.com](mailto:likailong@huawei.com)|
|32|[https://gitee.com/lijiarun](https://gitee.com/lijiarun)|[lijiarun@huawei.com](mailto:lijiarun@huawei.com)|
|33|[https://gitee.com/lijj01](https://gitee.com/lijj01)|[lijunjie29@huawei.com](mailto:lijunjie29@huawei.com)|
|34|[https://gitee.com/liuyoufang](https://gitee.com/liuyoufang)|[liuyu82@huawei.com](mailto:liuyu82@huawei.com)|
|35|[https://gitee.com/liuyuehua1](https://gitee.com/liuyuehua1)|[liuyuehua1@huawei.com](mailto:liuyuehua1@huawei.com)|
|36|[https://gitee.com/locheng7](https://gitee.com/locheng7)|[luocheng2@huawei.com](mailto:luocheng2@huawei.com)|
|37|[https://gitee.com/lv-zhongwei](https://gitee.com/lv-zhongwei)|[lvzhongwei@huawei.com](mailto:lvzhongwei@huawei.com)|
|38|[https://gitee.com/lz-230](https://gitee.com/lz-230)|[lizheng2@huawei.com](mailto:lizheng2@huawei.com)|
|39|[https://gitee.com/maplestorys](https://gitee.com/maplestorys)|[zengzhi5@huawei.com](mailto:zengzhi5@huawei.com)|
|40|[https://gitee.com/mengjingzhimo](https://gitee.com/mengjingzhimo)|[luoboming@huawei.com](mailto:luoboming@huawei.com)|
|41|[https://gitee.com/neeen](https://gitee.com/neeen)|[neen.yang@huawei.com](mailto:neen.yang@huawei.com)|
|42|[https://gitee.com/northking-super](https://gitee.com/northking-super)|[chenxin100@huawei.com](mailto:chenxin100@huawei.com)|
|43|[https://gitee.com/ohos-lsw](https://gitee.com/ohos-lsw)|[lishiwei6@huawei.com](mailto:lishiwei6@huawei.com)|
|44|[https://gitee.com/pssea](https://gitee.com/pssea)|[lizhiqi1@huawei.com](mailto:lizhiqi1@huawei.com)|
|45|[https://gitee.com/rain_myf](https://gitee.com/rain_myf)|[maoyufeng3@huawei.com](mailto:maoyufeng3@huawei.com)|
|46|[https://gitee.com/scuteehuangjun](https://gitee.com/scuteehuangjun)|[huangjun42@huawei.com](mailto:huangjun42@huawei.com)|
|47|[https://gitee.com/starr666](https://gitee.com/starr666)|[starr.zhang@huawei.com](mailto:starr.zhang@huawei.com)|
|48|[https://gitee.com/stesen](https://gitee.com/stesen)|[stesen.ma@huawei.com](mailto:stesen.ma@huawei.com)|
|49|[https://gitee.com/sunjunxiong](https://gitee.com/sunjunxiong)|[sunjunxiong@huawei.com](mailto:sunjunxiong@huawei.com)|
|50|[https://gitee.com/verystone](https://gitee.com/verystone)|[xudaqing@huawei.com](mailto:xudaqing@huawei.com)|
|51|[https://gitee.com/wangmihu2008](https://gitee.com/wangmihu2008)|[wangmihu@huawei.com](mailto:wangmihu@huawei.com)|
|52|[https://gitee.com/wangxing-hw](https://gitee.com/wangxing-hw)|[raymond.wangxing@huawei.com](mailto:raymond.wangxing@huawei.com)|
|53|[https://gitee.com/wangzaishang](https://gitee.com/wangzaishang)|[wangzaishang@huawei.com](mailto:wangzaishang@huawei.com)|
|54|[https://gitee.com/widecode](https://gitee.com/widecode)|[wanghancai@huawei.com](mailto:wanghancai@huawei.com)|
|55|[https://gitee.com/xautosoft](https://gitee.com/xautosoft)|[zhuwenchao@huawei.com](mailto:zhuwenchao@huawei.com)|
|56|[https://gitee.com/xuwenfang](https://gitee.com/xuwenfang)|[xuwenfang@huawei.com](mailto:xuwenfang@huawei.com)|
|57|[https://gitee.com/Xuyongpan](https://gitee.com/Xuyongpan)|[xuyongpan@huawei.com](mailto:xuyongpan@huawei.com)|
|58|[https://gitee.com/yaomanhai](https://gitee.com/yaomanhai)|[yaomanhai@huawei.com](mailto:yaomanhai@huawei.com)|
|59|[https://gitee.com/ye-xiangbin](https://gitee.com/ye-xiangbin)|[yexiangbin@huawei.com](mailto:yexiangbin@huawei.com)|
|60|[https://gitee.com/yinyouzhan](https://gitee.com/yinyouzhan)|[yinyouzhan@huawei.com](mailto:yinyouzhan@huawei.com)|
|61|[https://gitee.com/yuanwhong](https://gitee.com/yuanwhong)|[yuanwenhong@hisilicon.com](mailto:yuanwenhong@hisilicon.com)|
|62|[https://gitee.com/zhangadong](https://gitee.com/zhangadong)|[zhangadong@huawei.com](mailto:zhangadong@huawei.com)|
|63|[https://gitee.com/zhangzhiwi](https://gitee.com/zhangzhiwi)|[weizhi.zhang@huawei.com](mailto:weizhi.zhang@huawei.com)|
|64|[https://gitee.com/zhengbin5](https://gitee.com/zhengbin5)|[zhengbin5@huawei.com](mailto:zhengbin5@huawei.com)|
|65|[https://gitee.com/zhu-mingliang](https://gitee.com/zhu-mingliang)|[zhumingliang@huawei.com](mailto:zhumingliang@huawei.com)|
|66|[https://gitee.com/zianed](https://gitee.com/zianed)|[houxuanzhe@huawei.com](mailto:houxuanzhe@huawei.com)|