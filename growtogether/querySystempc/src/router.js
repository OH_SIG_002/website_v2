import Vue from "vue";
import VueRouter from "vue-router";

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};
Vue.use(VueRouter);

let routes = [
  {
    path: "/login",
    component: () => import("./components/login.vue"),
    meta: {
      nologin: true,
    },
  },
  {
    path: "/",
    redirect: "login",
  },
  {
    path: "/",
    component: () => import("./components/index.vue"),
    children: [
      {
        path: "mainTest",
        component: () => import("./components/mainTest.vue"),
      },
      {
        path: "detailCancel",
        component: () => import("./components/detailCancel.vue"),
      },
      {
        path: "detail",
        component: () => import("./components/detail.vue"),
      },
    ],
  },
];

let router = new VueRouter({
  mode: "history",
  routes,
});
export default router;
