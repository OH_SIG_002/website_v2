package com.chinasoft.mapper;

import com.chinasoft.bean.po.UserSubject;
import com.chinasoft.bean.vo.SignUpVO;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import java.util.Map;

@Mapper
public interface SignUpMapper {
        void saveSignUp(SignUpVO signUpVO);
        List<Map<String,Object>> getSignUpId();
        void saveUserSubject(UserSubject userSubject);
        List<Map<String,Object>> getSubjectByCategory(String project);
        List<Map<String,Object>> getStatusBySubjectId(String subjectId);
        List<Map<String,Object>> getSignBySubject(String subjectId);
        void updateSignUpStatus(String subjectId,String signUpStatus,String userId);
        List<Map<String,Object>> getSingnByPhone(String phone);
        List<Map<String,Object>> getSingStatusBySingId(String singId);
        List<Map<String,Object>> getUserSubject(String userId);
        List<Map<String,Object>> getSubject(String subjectId,String userId);
        void saveKnotImage(String userId,String knotImage);
        List<Map<String,Object>> getSignTime(String userId);
}
