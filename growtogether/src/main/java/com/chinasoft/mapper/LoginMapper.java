package com.chinasoft.mapper;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import java.util.Map;

@Mapper
public interface LoginMapper {
        List<Map<String,Object>> getBackstageLogin(String userName, String phone);
        List<Map<String,Object>> centerLogin(String userName,String phone);
}
