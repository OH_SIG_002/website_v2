package com.chinasoft.bean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserSubjectVO {
    private String subjectId;
    private String signUpStatus;
    private String userId;

}
