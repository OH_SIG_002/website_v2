package com.chinasoft.bean.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserSubject {

    private int id;
    private int signUpId;
    private int subjectId;
    private int signUpStatus;


}
