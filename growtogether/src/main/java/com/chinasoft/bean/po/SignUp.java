package com.chinasoft.bean.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignUp {

    private int id;
    private String userName;
    private String school;
    private String professional;
    private String address;
    private String  phoneNumber;
    private String giteeAccount;
    private String email;
    private String studentImage;
    private String knotImage;
    private Date signTime;

}
