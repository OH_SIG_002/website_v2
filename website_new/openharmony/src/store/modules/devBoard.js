import { queryDevelopment } from "../../api/index"
export default {
    state: {
        developmentList: [],
    },
    getters: {

    },
    mutations: {
        getDevelopment(state, data) {
            state.developmentList = data;
            // console.log("开发板", data);
            // console.log(state.developmentList);
        },

    },
    actions: {
        async getDevelopment({ commit }, type) {

            const { data } = await queryDevelopment();
            if (data.code === 0) {
                console.log("开发板", data);
                commit("getDevelopment", data.data);
            }
        },

    }
}