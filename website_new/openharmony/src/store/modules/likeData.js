import { updateCount } from "../../api/index"
export default {
    state: {
        //点赞
        liveList: [],
        //分享
        shareList: [],
        //浏览
        browseList: []
    },
    getters: {

    },
    mutations: {
        getUpdateCount(state, { list, typeId }) {
            switch (typeId) {
                case "1":
                case 1:
                    state.browseList = list;
                    break;
                case "2":
                case 2:
                    state.liveList = list;
                    console.log("state.liveList", state.liveList);
                    break;
                case "3":
                case 3:
                    state.shareList = list;
                    console.log("state.shareList", state.shareList);
                    break;
                default:
                    break;
            }
        },

    },
    actions: {
        async getUpdateCount({ commit }, { data, typeId }) {
            const { res } = await updateCount({ data });
            if (res.code === 0) {
                // console.log("你好", data);
                commit("getUpdateCount", { list: res.data, typeId });
            }
        },

    }
}