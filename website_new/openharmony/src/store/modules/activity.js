import { queryInformation } from "../../api/index"
export default {
    state: {
        informationList: [],
    },
    getters: {

    },
    mutations: {
        getInformation(state, data) {
            state.informationList = data;
            console.log(state.informationList);
        },

    },
    actions: {
        async getInformation({ commit }, type) {

            const { data } = await queryInformation(type);
            if (data.code === 0) {
                console.log("活动", data);
                commit("getInformation", data.data);
            }
        },
    }
}