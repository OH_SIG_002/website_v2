/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.service;

import com.alibaba.fastjson.JSONObject;
import com.chinasoft.bean.po.BusType;

import java.util.List;

public interface BusTypeService {
    int add(BusType busType);
    int edit(BusType busType);
    int remove(Integer id);
    int removeBatch(String ids);
    BusType find(BusType busType);
    List<BusType> findAll(BusType busType);
    List<BusType> findBatch(BusType busType);
    int findTotal(BusType busType);
    JSONObject getAllData();
}
