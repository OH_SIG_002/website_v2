/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.service.impl;

import com.chinasoft.bean.po.HomeContent;
import com.chinasoft.bean.po.HomeContentPic;
import com.chinasoft.bean.po.HomeDevelopment;
import com.chinasoft.mapper.SampleMapper;
import com.chinasoft.mapper.SecondaryPageMapper;
import com.chinasoft.service.SecondaryPageService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SecondaryPageServiceImpl implements SecondaryPageService {

    private static Logger logger = Logger.getLogger(SecondaryPageServiceImpl.class);

    @Autowired
    private SecondaryPageMapper secondaryPageMapper;

    @Override
    public List<HomeContentPic> queryCarousel(int type,String category) {
        return secondaryPageMapper.queryCarousel(type,category);
    }

    @Override
    public List<HomeContent> queryBatch(HomeContent content) {
        return secondaryPageMapper.queryBatch(content);
    }

    @Override
    public int queryTotal(HomeContent content) {
        return secondaryPageMapper.queryTotal(content);
    }

    @Override
    public HomeContent queryDetails(int id) {
        return secondaryPageMapper.queryDetails(id);
    }

    @Override
    public int updateCount(int id, int type) {
        HomeContent homeContent = new HomeContent();
        homeContent.setId(id);
        homeContent.setType(type);
        return secondaryPageMapper.updateCount(homeContent);
    }

    @Override
    public List<HomeDevelopment> queryDevelopmentBatch(HomeDevelopment dept) {
        return secondaryPageMapper.queryDevelopmentBatch(dept);
    }

    @Override
    public int queryDevelopmentTotal(HomeDevelopment dept) {
        return secondaryPageMapper.queryDevelopmentTotal(dept);
    }
}
