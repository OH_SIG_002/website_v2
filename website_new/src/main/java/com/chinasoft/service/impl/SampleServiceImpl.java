/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.chinasoft.bean.vo.AddSampleVo;
import com.chinasoft.bean.vo.BaseInfoListVo;
import com.chinasoft.bean.vo.QuerySampleVo;
import com.chinasoft.bean.vo.TypeDetailListVo;
import com.chinasoft.mapper.SampleMapper;
import com.chinasoft.service.SampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class SampleServiceImpl implements SampleService {

    @Autowired
    private SampleMapper sampleMapper;

    @Override
    public ArrayList<QuerySampleVo> querySample(HashMap<String, String> params) {
        return sampleMapper.querySample(params);
    }

    @Override
    public JSONObject queryInformation(String busTypeId) {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("featuresList",sampleMapper.queryFeatures(busTypeId));
        jsonObject.put("detailTypeList",sampleMapper.queryDetailType());
        return jsonObject;
    }

    @Override
    public List<Map<String, Object>> queryData() {
        return sampleMapper.queryData();
    }

    @Override
    public int queryPageNum(HashMap<String, String> params) {
        return sampleMapper.queryPageNum(params);
    }

    @Override
    public void addSample(AddSampleVo addSampleVo) {
        //新增开发样例详情
        sampleMapper.addSample(addSampleVo);
        int sampleId=addSampleVo.getId();
        //新增开发样例基本信息
        List<BaseInfoListVo> baseInfoList=addSampleVo.getBaseInfoList();
        if (null != baseInfoList){
            for(int i=0;i<baseInfoList.size();i++){
                BaseInfoListVo baseInfoListVo=new BaseInfoListVo();
                baseInfoListVo.setDeviceLevel(baseInfoList.get(i).getDeviceLevel());
                baseInfoListVo.setSysVersion(baseInfoList.get(i).getSysVersion());
                baseInfoListVo.setVersionNo(baseInfoList.get(i).getVersionNo());
                baseInfoListVo.setSampleId(sampleId);
                sampleMapper.addBaseInfo(baseInfoListVo);
            }
        }

        //新增开发样例相关资料
        List<TypeDetailListVo> typeDetailList=addSampleVo.getTypeDetailList();
        if (null != typeDetailList){
            for(int i=0;i<typeDetailList.size();i++){
                TypeDetailListVo typeDetailListVo=new TypeDetailListVo();
                typeDetailListVo.setTypeId(typeDetailList.get(i).getTypeId());
                typeDetailListVo.setName(typeDetailList.get(i).getName());
                typeDetailListVo.setDescription(typeDetailList.get(i).getDescription());
                typeDetailListVo.setUrl(typeDetailList.get(i).getUrl());
                typeDetailListVo.setSampleId(sampleId);
                sampleMapper.addTypeDetail(typeDetailListVo);
            }
        }
    }

    @Override
    public void updateSample(AddSampleVo addSampleVo) {
        sampleMapper.updateSample(addSampleVo);
        //编辑开发样例时，基本信息与相关资料先删后增
        sampleMapper.deleteBaseInfo(addSampleVo);
        sampleMapper.deleteTypeDetail(addSampleVo);
        //新增开发样例基本信息
        List<BaseInfoListVo> baseInfoList=addSampleVo.getBaseInfoList();
        for(int i=0;i<baseInfoList.size();i++){
            BaseInfoListVo baseInfoListVo=new BaseInfoListVo();
            baseInfoListVo.setDeviceLevel(baseInfoList.get(i).getDeviceLevel());
            baseInfoListVo.setSysVersion(baseInfoList.get(i).getSysVersion());
            baseInfoListVo.setVersionNo(baseInfoList.get(i).getVersionNo());
            baseInfoListVo.setSampleId(Integer.parseInt(addSampleVo.getSampleId()));
            sampleMapper.addBaseInfo(baseInfoListVo);
        }
        //新增开发样例相关资料
        List<TypeDetailListVo> typeDetailList=addSampleVo.getTypeDetailList();
        for(int i=0;i<typeDetailList.size();i++){
            TypeDetailListVo typeDetailListVo=new TypeDetailListVo();
            typeDetailListVo.setTypeId(typeDetailList.get(i).getTypeId());
            typeDetailListVo.setName(typeDetailList.get(i).getName());
            typeDetailListVo.setDescription(typeDetailList.get(i).getDescription());
            typeDetailListVo.setUrl(typeDetailList.get(i).getUrl());
            typeDetailListVo.setSampleId(Integer.parseInt(addSampleVo.getSampleId()));
            sampleMapper.addTypeDetail(typeDetailListVo);
        }
    }

    @Override
    public void deleteSample(AddSampleVo addSampleVo) {
        sampleMapper.deleteSample(addSampleVo);
        sampleMapper.deleteBaseInfo(addSampleVo);
        sampleMapper.deleteTypeDetail(addSampleVo);
    }
}

