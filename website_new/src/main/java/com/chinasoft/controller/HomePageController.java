/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.controller;

import com.chinasoft.annotation.OperationLog;
import com.chinasoft.service.HomePageService;
import com.chinasoft.utils.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 1230官网首页
 * @author dengtao
 */

@CrossOrigin
@RestController
@RequestMapping("/homePage")
@Api(tags = "首页",description = "1230官网首页")
public class HomePageController {

    private static final Logger logger = LoggerFactory.getLogger(HomePageController.class);

    @Autowired
    private HomePageService homePageService;

    @ApiOperation(value = "首页轮播图")
    @GetMapping("/queryBanner")
    @OperationLog(action = "查询", operateType = "query")
    public ResultVO queryBanner(@RequestParam("category") String category) {
        ResultVO resultVo = new ResultVO();
        try {
            resultVo.setData(homePageService.queryBanner(category));
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("查询成功");
        } catch (Exception e) {
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "首页会议")
    @GetMapping("/queryMeeting")
    @OperationLog(action = "查询", operateType = "query")
    public ResultVO queryMeeting(@RequestParam("date") String date) {
        ResultVO resultVo = new ResultVO();
        try {
            resultVo.setData(homePageService.queryMeeting(date));
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("查询成功");
        } catch (Exception e) {
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "首页活动、博客、新闻")
    @GetMapping("/queryInformation")
    @OperationLog(action = "查询", operateType = "query")
    public ResultVO queryInformation(@RequestParam("type") String type) {
        ResultVO resultVo = new ResultVO();
        try {
            resultVo.setData(homePageService.queryInformation(type));
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("查询成功");
        } catch (Exception e) {
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "首页开发板")
    @GetMapping("/queryDevelopment")
    @OperationLog(action = "查询", operateType = "query")
    public ResultVO queryDevelopment() {
        ResultVO resultVo = new ResultVO();
        try {
            resultVo.setData(homePageService.queryDevelopment());
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("查询成功");
        } catch (Exception e) {
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }
    @ApiOperation(value = "首页厂商链接   ")
    @GetMapping("/queryVendor")
    @OperationLog(action = "查询", operateType = "query")
    public ResultVO queryVendor() {
        ResultVO resultVo = new ResultVO();
        try {
            resultVo.setData(homePageService.queryVendor());
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("查询成功");
        } catch (Exception e) {
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }
}
