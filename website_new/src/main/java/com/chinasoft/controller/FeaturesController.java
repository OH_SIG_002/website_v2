/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.controller;

import com.chinasoft.annotation.OperationLog;
import com.chinasoft.bean.po.Features;
import com.chinasoft.bean.vo.QuerySampleVo;
import com.chinasoft.service.FeaturesService;
import com.chinasoft.utils.PageListVO;
import com.chinasoft.utils.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 特性模块
 *
 * @author dengtao
 */
@CrossOrigin
@RestController
@RequestMapping("/features")
@Api(tags = "特性", description = "特性管理")
public class FeaturesController {

    private static final Logger logger = LoggerFactory.getLogger(FeaturesController.class);

    @Autowired
    private FeaturesService featuresService;

    @ApiOperation(value = "特性信息保存")
    @PostMapping("/addFeatures")
    @OperationLog(action = "特性信息[保存]", operateType = "features/save")
    public ResultVO save(@RequestBody Features features) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null != features) {
                Features featuresVO = featuresService.find(features);
                if (null != featuresVO) {
                    resultVo.setCode(ResultVO.FAIL);
                    resultVo.setMsg("特性名称已存在");
                    return resultVo;
                }
            }
            int res = featuresService.add(features);
            logger.info("success > 0,failed <= 0, save result=" + res);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("新增成功");
            resultVo.setData(res);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }


    @ApiOperation(value = "特性信息修改")
    @PostMapping("/editFeatures")
    @OperationLog(action = "特性信息[修改]", operateType = "features/edit")
    public ResultVO edit(@RequestBody Features features) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == features) {
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("参数为空");
                return resultVo;
            }
            int res = featuresService.edit(features);
            logger.info("success > 0,failed <= 0, save result=" + res);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("编辑成功");
            resultVo.setData(res);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "特性信息删除")
    @PostMapping("/delFeatures")
    @OperationLog(action = "特性信息[删除]", operateType = "features/remove")
    public ResultVO remove(@RequestBody Features features) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == features || features.getFeaturesId() == 0) {
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("参数为空");
                return resultVo;
            }
            int res = featuresService.remove(features.getFeaturesId());
            logger.info("success > 0,failed <= 0, save result=" + res);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("删除成功");
            resultVo.setData(res);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "特性信息批量删除")
    @GetMapping("/removeBatch")
    @OperationLog(action = "特性信息[批量删除]", operateType = "features/removeBatch")
    public ResultVO removeBatch(@RequestParam(value = "ids", required = false) String ids) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == ids || "".equals(ids)) {
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("ids or proIds is null");
                return resultVo;
            }
            int res = featuresService.removeBatch(ids);
            logger.info("success > 0,failed <= 0, save result=" + res);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("remove batch features success");
            resultVo.setData(res);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "特性信息单个查看")
    @GetMapping("/find")
    @OperationLog(action = "特性信息[单个查看]", operateType = "features/find")
    public ResultVO find(Features features) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == features) {
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("param is null");
                return resultVo;
            }
            Features featuresVO = featuresService.find(features);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("find features success");
            resultVo.setData(featuresVO);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    /**
     * 分页查询
     *
     * @param features
     * @return pageListVO
     */
    @ApiOperation(value = "特性信息分页查询")
    @GetMapping("/queryFeatures")
    @OperationLog(action = "特性信息[分页查询]", operateType = "features/findBatch")
    public PageListVO<List<QuerySampleVo>> findBatch(Features features) {
        PageListVO<List<QuerySampleVo>> pageListVO = new PageListVO<>();
        if (null == features) {
            features = new Features();
        }
        if (null == features.getPageNum() || features.getPageNum() <= 0) {
            features.setPageNum(1);
        }
        if (null == features.getPageSize()) {
            features.setPageSize(10);
        }
        List<QuerySampleVo> resultList = featuresService.findBatch(features);
        Integer resultTotal = featuresService.findTotal(features);
        int page = resultTotal / features.getPageSize() + (resultTotal % features.getPageSize() > 0 ? 1 : 0);
        pageListVO.setTotalPage(page);
        pageListVO.setData(resultList);
        pageListVO.setCode(PageListVO.SUCCESS);
        pageListVO.setTotalNum(resultTotal);
        pageListVO.setPageNum(features.getPageNum());
        pageListVO.setPageSize(features.getPageSize());
        return pageListVO;
    }

}
