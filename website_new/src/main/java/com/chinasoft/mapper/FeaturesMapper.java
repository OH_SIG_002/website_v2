/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.mapper;

import com.chinasoft.bean.po.Features;
import com.chinasoft.bean.vo.QuerySampleVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FeaturesMapper {
    int insert(Features features);
    int update(Features features);
    int delete(Integer id);
    int deleteBatch(List<Integer> ids);
    Features query(Features features);
    List<Features> queryAll(Features features);
    List<QuerySampleVo> queryBatch(Features features);
    int queryTotal(Features features);

}
