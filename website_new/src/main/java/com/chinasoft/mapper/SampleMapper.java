/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.mapper;

import com.chinasoft.bean.vo.AddSampleVo;
import com.chinasoft.bean.vo.BaseInfoListVo;
import com.chinasoft.bean.vo.QuerySampleVo;
import com.chinasoft.bean.vo.TypeDetailListVo;
import org.apache.ibatis.annotations.Mapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mapper
public interface SampleMapper {
    ArrayList<QuerySampleVo> querySample(HashMap<String, String> params);
    List<Map<String,Object>> queryFeatures(String busTypeId);
    List<Map<String,Object>> queryDetailType();
    List<Map<String,Object>> queryData();
    int queryPageNum(HashMap<String, String> params);
    int addSample(AddSampleVo addSampleVo);
    int addBaseInfo(BaseInfoListVo baseInfoListVo);
    int addTypeDetail(TypeDetailListVo typeDetailListVo);
    int updateSample(AddSampleVo addSampleVo);
    void deleteSample(AddSampleVo addSampleVo);
    void deleteBaseInfo(AddSampleVo addSampleVo);
    void deleteTypeDetail(AddSampleVo addSampleVo);
}
