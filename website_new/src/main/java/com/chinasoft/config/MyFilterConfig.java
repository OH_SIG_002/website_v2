/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.config;


import com.chinasoft.filter.MyFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.servlet.Filter;

/**
 * 注册过滤器容器到servlet
 *
 * @author 杨一康
 *
 */


@Configuration
public class MyFilterConfig {
    @Bean
    public FilterRegistrationBean someFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(myFilter());
        registration.addUrlPatterns("/*");
        registration.setName("myFilter");
        return registration;
    }

    @Bean(name = "myFilter")
    public Filter myFilter() {
        return new MyFilter();
    }

    @Bean(name = "BCryptPasswordEncoder")
    public BCryptPasswordEncoder myBCryptPasswordEncoder(){
        return  new BCryptPasswordEncoder();
    }
}
