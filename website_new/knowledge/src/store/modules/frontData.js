export default {
    state: {
        breadLine: [
            { id: 0, val: "应用开发", name: "content", flagId: 0 },
            { id: 1, val: "入门级", name: "mainText", flagId: 0 },
            { id: 2, val: "UI", name: "allFeatures", flagId: 0 },
        ],
    },
    mutations: {
        getBreadData(state, { curentName, flagLevel, flagId }) {
            // console.log("8888888888888888", curentName);
            if (flagLevel === "all") {
                if (state.breadLine.length > 3) {
                    state.breadLine.pop();
                }
                let str = {};
                str.id = 3;
                str.val = curentName;
                str.name = "detail";
                state.breadLine.push(str);
            } else {
                // console.log("////////////////////", state.breadLine.length)
                if (state.breadLine.length > 3) {
                    state.breadLine.pop();
                }
                state.breadLine[flagLevel].val = curentName;
                state.breadLine[flagLevel].val = curentName;
                switch (flagLevel) {
                    case 0:
                        state.breadLine[flagLevel].flagId = flagId;
                        break;
                    case 1:
                        state.breadLine[flagLevel].flagId = flagId;
                        break;
                    case 2:
                        state.breadLine[flagLevel].flagId = flagId;
                        break;
                    default:
                        break;
                }
            }

        }
    },
    actions: {
        getBreadData({ commit }, { curentName, flagLevel, flagId }) {
            // const { data, code } = await getMark();
            // console.log(curentName);
            commit("getBreadData", { curentName, flagLevel, flagId })
        },
    }
}