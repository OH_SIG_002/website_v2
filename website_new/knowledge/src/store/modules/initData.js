export default {
  state: {
    // ui 媒体
    busTypeList: [],
    // js ui
    featuresList: [],
    // 级别 入门
    levelList: [],
    // 数据
    sampleList: [],
    // 应用开发 设备开发
    titleList: [],
    //OS类型
    osTypeList: [],
    //OS版本
    osVersionList: [],
    pageSize: 9,
    pageNum: 1,
    totalNum: 0,
  },
  mutations: {
    getInitData(
      state,
      {
        busTypeList,
        featuresList,
        sampleList,
        titleList,
        osTypeList,
        osVersionList,
        totalNum,
      }
    ) {
      state.sampleList = [];
      state.busTypeList = busTypeList;
      state.sampleList = sampleList;
      state.titleList = titleList;
      state.osTypeList = osTypeList;
      state.osVersionList = osVersionList;
      state.totalNum = totalNum;

      if (state.busTypeList[0]) {
        localStorage.setItem("busTypeId", state.busTypeList[0].busTypeId);
      }
      if (featuresList != null && featuresList.length != 0) {
        let str = {
          featuresId: 0,
          name: "全部特性",
        };
        state.featuresList = featuresList;
        state.featuresList.unshift(str);
      } else if (featuresList === null) {
        state.featuresList = [];
      }
    },
    getPageData(
      state,
      {
        busTypeList,
        featuresList,
        sampleList,
        titleList,
        osTypeList,
        osVersionList,
        pageSize,
        pageNum,
        totalNum,
      }
    ) {
      state.sampleList = [];
      state.busTypeList = busTypeList;
      state.sampleList = sampleList;
      state.titleList = titleList;
      state.osTypeList = osTypeList;
      state.osVersionList = osVersionList;
      state.pageSize = pageSize;
      state.pageNum = pageNum;
      state.totalNum = totalNum;
      // if (state.busTypeList[0]) {
      //     localStorage.setItem("busTypeId", state.busTypeList[0].busTypeId);
      //     console.log("11111111111111111", state.busTypeList[0].busTypeId);
      // }
      if (featuresList != null && featuresList.length != 0) {
        let str = {
          featuresId: 0,
          name: "全部特性",
        };
        state.featuresList = featuresList;
        state.featuresList.unshift(str);
      } else {
        state.featuresList = [];
      }
    },
  },
  actions: {
    getInitData(
      { commit },
      {
        busTypeList,
        featuresList,
        sampleList,
        titleList,
        osTypeList,
        osVersionList,
        totalNum,
      }
    ) {
      commit("getInitData", {
        busTypeList,
        featuresList,
        sampleList,
        titleList,
        osTypeList,
        osVersionList,
        totalNum,
      });
    },
    getPageData(
      { commit },
      {
        busTypeList,
        featuresList,
        sampleList,
        titleList,
        osTypeList,
        osVersionList,
        pageSize,
        pageNum,
        totalNum,
      }
    ) {
      commit("getPageData", {
        busTypeList,
        featuresList,
        sampleList,
        titleList,
        osTypeList,
        osVersionList,
        pageSize,
        pageNum,
        totalNum,
      });
    },
  },
};
